import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import './App.css';
import Welcome from './containers/WelcomeContainer';
import Learning from './containers/LearningContainer';
import Score from './containers/ScoreContainer';
import Dictionary from './containers/DictionaryContainer';
import SignInUpSide from './containers/LoginContainer';



class App extends Component {
  render() {
    return (
      <div>
        <Switch>
          <Route path="/" exact component={SignInUpSide}/>
          <Route path="/welcome"  component={Welcome}/>
          <Route path="/learning" component={Learning}/>
          <Route path="/score" component={Score}/>
          <Route path="/dictionary" component={Dictionary}/>
        </Switch>
      </div>
    );
  }
}

export default App;
