import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
// import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo';
import { AUTH_TOKEN, LOGIN_ERROR, LOGIN_PASS_ERROR} from '../constants';

import { 
    LOGIN,
    SIGNUP,
    storeAuth
} from './Mutation';

import Login from '../components/Login';
import Signup from '../components/Signup';

const styles = theme => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://source.unsplash.com/random)',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    // margin: theme.spacing(8, 4),
    marginTop: theme.spacing.unit * 8,
    marginBottom: theme.spacing.unit * 4,
    marginLeft: theme.spacing.unit * 4,
    marginRight: theme.spacing.unit * 4,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  submit: {
    marginTop: theme.spacing.unit * 3,
    marginBottom: theme.spacing.unit * 2,
  },
  bottomText: {
    marginTop: theme.spacing.unit * 4,
  },
  fieldsLeft: {
    paddingRight: theme.spacing.unit,
    paddingBottom: theme.spacing.unit * 3,
    marginTop: theme.spacing.unit
  },
  fieldsRight: {
    marginTop: theme.spacing.unit,
    paddingBottom: theme.spacing.unit * 3
  },
  fullwidthFields: {
      paddingBottom: theme.spacing.unit * 3
  }
});

class SignInUpSide extends Component {
    constructor(props) {
        super(props);
        this.state = {
            signIn: true,
            firstName:'',
            lastName:'',
            email:'',
            password:'',
            emailError:'',
            passwordError:''
        }
    }

    MadeWithLove = () => {
        return (
          <Typography variant="body2" color="textSecondary" align="center">
            {'Built with love by the '}
            <Link color="inherit" href="https://material-ui.com/">
              Material-UI
            </Link>
            {' team.'}
          </Typography>
        );
    }

    changeUi = (signIn) => {
        this.setState({
            signIn: signIn
        })
    }

    onChange = (name,event) => {
        localStorage.setItem(LOGIN_ERROR, '')
        localStorage.setItem(LOGIN_PASS_ERROR, '')
        this.setState({
            [name]: event.target.value,
            emailError: '',
            passwordError:''
        })
    };

    handleSubmit = async event => {
        event.preventDefault();
        const { LOGIN, storeAuth } = this.props;
        try {
            const login = await LOGIN({
                variables: { email: this.state.email, password: this.state.password }
            });
            const storeToken = await storeAuth({
                variables: { email: login.data.login.user.email, token: login.data.login.token }
            })
            localStorage.setItem(AUTH_TOKEN, login.data.login.token)
            this.props.history.push('/welcome');
        } catch (error) {
            const emailErrorMessage = localStorage.getItem(LOGIN_ERROR)
            const passwordErrorMessage = localStorage.getItem(LOGIN_PASS_ERROR)
            if(emailErrorMessage.length > 0) {
                this.setState({
                    emailError: emailErrorMessage
                })
            }
            if(passwordErrorMessage.length > 0) {
                this.setState({
                    passwordError: passwordErrorMessage
                })
            }
        }
    }

    handleSignUpSubmit = async event => {
        event.preventDefault();
        const { SIGNUP, storeAuth } = this.props;
        const { email,  password, firstName, lastName} = this.state;
        const signup = await SIGNUP({
            variables: { email: email, password: password, name: firstName + ' ' + lastName}
        });
        const storeToken = await storeAuth({
            variables: { email: signup.data.signup.user.email, token: signup.data.signup.token }
        })
        localStorage.setItem(AUTH_TOKEN, signup.data.signup.token)
        this.props.history.push('/welcome');
    }

    render() {
        const { classes } = this.props;
        const { signIn, email, password, emailError, passwordError } = this.state;
        return (
            <div>
            <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                   {signIn ? 'Sign in' : 'Sign Up'} 
                </Typography>
                {
                    signIn ? 
                    <Login handleSubmit={this.handleSubmit} onChange={this.onChange} email={email} password={password} classes={classes} change={this.changeUi} signIn={signIn} emailError={emailError} passwordError={passwordError}/> 
                    : 
                    <Signup handleSubmit={this.handleSignUpSubmit} onChange={this.onChange} classes={classes} change={this.changeUi} signIn={signIn}/>
                }
                </div>
            </Grid>
            </Grid>
            </div>
        );
    }
}

export default withRouter(
    compose (
        graphql(LOGIN, { name: 'LOGIN' }),
        graphql(storeAuth, { name: 'storeAuth' }),  
        graphql(SIGNUP, { name: 'SIGNUP' })
    )(withStyles(styles)(SignInUpSide)))