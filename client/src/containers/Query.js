import gql from 'graphql-tag';

const FETCH_QUERY = gql`
query FETCH_QUERY {
    kanjis {
      id
      character
      hiragana 
  }
}
`

const fetch_query_local = gql`
query fetch_query_local {
    kanjis @client{
      id
      character
      hiragana 
  }
}
`
const FETCH_SELECTED_QUERY = gql`
query FETCH_SELECTED_QUERY {
  pickKanjis {
      character
      options 
      correctIndex
  }
}
`
const currentCount = gql`
query currentCount {
  count @client
}
`
const currentTotalTime = gql`
query currentTotalTime {
  totalTime @client
}
`
const currentCorrectCount = gql`
query currentCorrectCount {
  correctCount @client
}
`
const gameResults = gql`
query gameResults {
  correctCount @client
  totalTime @client
}
`
const fetch_token_local = gql`
query fetch_token_local {
  token @client
}
`

export {
    FETCH_QUERY,
    FETCH_SELECTED_QUERY,
    currentCount,
    currentTotalTime,
    currentCorrectCount,
    gameResults,
    fetch_query_local,
    fetch_token_local
}