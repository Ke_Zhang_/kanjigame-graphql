import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Header from '../components/Header';
import { withStyles } from '@material-ui/core/styles';
import '../App.css';
import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo';
import {
    FETCH_QUERY,
} from './Query';
import {
    ADD_KANJI,
    DELETE_KANJI,
    UPDATE_KANJI
} from './Mutation';
import MaterialTable from 'material-table';
import CircularProgress from '@material-ui/core/CircularProgress';
import gql from 'graphql-tag';
import Particles from 'react-particles-js';
import { withSnackbar } from 'notistack';
import { ADD_KANJI_ERROR } from '../constants';

const NEW_KANJI_SUBSCRIPTION = gql`
  subscription {
    newKanji {
      id
      character
      hiragana
    }
  }
`
const DELETE_KANJI_SUBSCRIPTION = gql`
  subscription {
    deletedKanji {
      id
      character
      hiragana
    }
  }
`


const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 200
    },
});

class Dictionary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [
                { title: 'Character', field: 'character' },
                { title: 'Hiragana', field: 'hiragana',  },
              ]
        }
    }

    _subscribeToNewKanji = subscribeToMore => {
        subscribeToMore({
          document: NEW_KANJI_SUBSCRIPTION,
          updateQuery: (prev, { subscriptionData }) => {
            const newKanji = subscriptionData.data.newKanji
            const exists = prev.kanjis.find(({ id }) => id === newKanji.id);
            if (exists) return prev;
            return Object.assign({}, prev, {
                kanjis: [...prev.kanjis, newKanji],
                __typename: 'Kanji'
            })
          }
        })
      }

      _subscribeToDeleteKanji = subscribeToDelete => {
        subscribeToDelete({
          document: DELETE_KANJI_SUBSCRIPTION,
          updateQuery: (prev, { subscriptionData }) => {
            const deleteKanji = subscriptionData.data.deletedKanji
            if (deleteKanji.id){
                return Object.assign({}, prev, {
                    kanjis: prev.kanjis.filter(value => value.id !== deleteKanji.id),
                    __typename: 'Kanji'
                })
            }
            return prev;
          }
        })
      }

      renderBackground = () => {
          return (
                <Particles
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }}
                    params={{
                        "particles": {
                            "number": {
                                "value": 160,
                                "density": {
                                    "enable": false
                                }
                            },
                            "size": {
                                "value": 3,
                                "random": true,
                                "anim": {
                                    "speed": 4,
                                    "size_min": 0.3
                                }
                            },
                            "line_linked": {
                                "enable": false
                            },
                            "move": {
                                "random": true,
                                "speed": 1,
                                "direction": "top",
                                "out_mode": "out"
                            }
                        },
                        "interactivity": {
                            "events": {
                                "onhover": {
                                    "enable": true,
                                    "mode": "bubble"
                                },
                                "onclick": {
                                    "enable": true,
                                    "mode": "repulse"
                                }
                            },
                            "modes": {
                                "bubble": {
                                    "distance": 250,
                                    "duration": 2,
                                    "size": 0,
                                    "opacity": 0
                                },
                                "repulse": {
                                    "distance": 400,
                                    "duration": 4
                                }
                            }
                        }
                    }} />
          )
      }

    render() {
        const { FETCH_QUERY, ADD_KANJI, DELETE_KANJI, UPDATE_KANJI } = this.props;
        const { columns } = this.state;
        if (FETCH_QUERY.loading){
            return (
                <div className='SpinerContainer'>
                    {this.renderBackground()}
                    <CircularProgress disableShrink />
                </div>
            )
        }
        const kanjis = FETCH_QUERY.kanjis;
        this._subscribeToNewKanji(FETCH_QUERY.subscribeToMore)
        this._subscribeToDeleteKanji(FETCH_QUERY.subscribeToMore)
        return (
            <div className='WelcomeContainer'>
                {
                    this.renderBackground()
                }
                <div>
                    <Header title='Kanji Dictionary' text='Back' to='/welcome'/>
                </div>
                <div className='Dictionary'>
                    <MaterialTable
                    title="Kanji Dictionary"
                    columns={columns}
                    data={kanjis}
                    editable={{
                        onRowAdd: newData =>
                        new Promise(async resolve => {
                            let variant = 'warning';
                            try {
                                const add = await ADD_KANJI({
                                    variables: { character: newData.character, hiragana: newData.hiragana }
                                });
                                resolve();
                                variant = 'success'
                                this.props.enqueueSnackbar('New Char successfully added', { variant })
                            } catch (error) {
                                resolve();
                                const message = localStorage.getItem(ADD_KANJI_ERROR)
                                this.props.enqueueSnackbar(message, { variant })
                            }
                        }),
                        onRowUpdate: (newData, oldData) =>
                        new Promise(async resolve => {
                            const update = await UPDATE_KANJI({
                                variables: { id: newData.id, character: newData.character, hiragana: newData.hiragana }
                            });
                            resolve();
                        }),
                        onRowDelete: oldData =>
                        new Promise(async resolve => {
                            const deleteRow = await DELETE_KANJI({
                                variables: { id: oldData.id }
                            });
                            resolve();
                        }), 
                    }}
                    />
                </div>
            </div>
        )
    }
}

Dictionary.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withSnackbar(withRouter(
    compose(
        graphql(FETCH_QUERY, { name: 'FETCH_QUERY' }),
        graphql(ADD_KANJI, { name: 'ADD_KANJI' }),
        graphql(DELETE_KANJI, { name: 'DELETE_KANJI' }),
        graphql(UPDATE_KANJI, { name: 'UPDATE_KANJI' }),
    )(withStyles(styles)(Dictionary))))