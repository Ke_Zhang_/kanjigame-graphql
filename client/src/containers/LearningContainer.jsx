import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Buttons from '../components/Buttons';
import Header from '../components/Header';
import Counter from '../components/Counter';
import TimeCounter from '../components/TimeCounter';
import Kanji from '../components/Kanji';
import Options from '../components/Options';
import { withStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import '../App.css';
import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo';
import { 
    FETCH_SELECTED_QUERY,
    currentCount,
    currentTotalTime,
    currentCorrectCount 
} from './Query';
import {
    increaseCount,
    setTotalTime,
    setCorrectCount
} from './Mutation';
import Particles from 'react-particles-js';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        height: 500,
        width: 1080
    },
    divider: {
    }
});
class Learning extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sec: 0,
            selectedSec: 0,
            shouldHighlight: false
        }
        this.enterPress = this.enterPress.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.currentCount.count > 9) {
            clearInterval(this.interval);
            this.props.history.push('/score');
        }
    }

    enterPress(event) {
        if (event.keyCode === 13) {
            this.handleClickOption(-1);
        }
    }

    componentDidMount() {
        document.addEventListener("keydown", this.enterPress, false);
        this.interval = setInterval(() => {
            if (this.state.sec === 25 && !this.state.shouldHighlight) {
                this.setState({ sec: this.state.sec + 1, shouldHighlight: true });
            } else {
                this.setState({ sec: this.state.sec + 1 });
            }
        }, 1000);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.enterPress, false);
    }

    componentDidUpdate() {
        if (this.state.sec === 30 || this.state.selectedSec === 5) {
            const { currentCount } = this.props;
            this.handleClickOption(currentCount.count + 1);
        }
    }

    handleClickOption = (data) => {
        clearTimeout(this.setTimeOut);
        const { increaseCount, setTotalTime, currentTotalTime } = this.props
        increaseCount({
            variables: { count: data }
        });
        setTotalTime({
            variables: { totalTime: currentTotalTime.totalTime + this.state.sec}
        });
        this.setState({ sec: 0, shouldHighlight: false })
    };

    handelSelectAnswer = (answer, correctAnswer, count) => {
        if (answer !== correctAnswer) {
            this.setState({ shouldHighlight: true })
            this.setTimeOut = setTimeout(() => {
                this.handleClickOption(count+1);
            },5000)
        } else {
            this.props.setCorrectCount({
                variables: { correctCount: this.props.currentCorrectCount.correctCount + 1 } 
            })
            this.handleClickOption(count+1);
        }
    }

    render() {
        const { classes, FETCH_SELECTED_QUERY, currentCount } = this.props;
        const { sec, shouldHighlight } = this.state;
        if (FETCH_SELECTED_QUERY.loading || currentCount.loading){
            return (
                <div className='SpinerContainer'>
                    <Particles
                        style={{
                            position: 'absolute',
                            left: 0,
                            right: 0,
                            bottom: 0,
                            top: 0
                        }}
                        params={{
                            "particles": {
                                "number": {
                                    "value": 50
                                },
                                "size": {
                                    "value": 3
                                }
                            },
                            "interactivity": {
                                "events": {
                                    "onhover": {
                                        "enable": true,
                                        "mode": "repulse"
                                    }
                                }
                            }
                        }} />
                    <CircularProgress disableShrink color="secondary"/>
                </div>
            )
        }
        if (currentCount.count > 9) {
            return null;
        }
        const kanji = FETCH_SELECTED_QUERY.pickKanjis;
        return (
            <div className='LearningContainer'>
                <Particles
                    style={{
                        position: 'absolute',
                        left: 0,
                        right: 0,
                        bottom: 0,
                        top: 0
                    }}
                    params={{
                        "particles": {
                            "number": {
                                "value": 50
                            },
                            "size": {
                                "value": 3
                            }
                        },
                        "interactivity": {
                            "events": {
                                "onhover": {
                                    "enable": true,
                                    "mode": "repulse"
                                }
                            }
                        }
                    }} />
                <div>
                    <Header title='Kanji Learning' text='Replay' to='/welcome'/>
                </div>
                <div className='LearningPaper'>
                    <Paper className={classes.root} elevation={1}>

                        <div className='kanjiPad'>
                            <Counter num={currentCount.count + 1} />
                            <div className='kanji'>
                                <Kanji text={kanji[currentCount.count].character} shouldHighlight={shouldHighlight} />
                            </div>
                            <div className='timeCounter'>
                                <TimeCounter sec={sec} />
                            </div>
                        </div>
                        <Divider variant="middle" className={classes.divider} />
                        <div className='options'>
                            {
                                kanji[currentCount.count].options.map((a, b) => {
                                    return <Options count={currentCount.count} text={a} key={b} answer={b} action={this.handelSelectAnswer} correctAnswer={kanji[currentCount.count].correctIndex} shouldHighlight={shouldHighlight} />
                                })
                            }
                        </div>
                        <Buttons text={shouldHighlight ? 'Next':'Skip' } color='primary' variant='contained' action={() => this.handleClickOption(currentCount.count + 1)} />
                    </Paper >
                </div>
            </div>
        )
    }
}

Learning.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withRouter(
    compose (
        graphql(FETCH_SELECTED_QUERY, { name: 'FETCH_SELECTED_QUERY' }), 
        graphql(currentCount, { name: 'currentCount' }),
        graphql(currentCorrectCount, {name: 'currentCorrectCount'}),
        graphql(increaseCount, {name: 'increaseCount'}), 
        graphql(currentCount, { name: 'currentCount' }),
        graphql(setTotalTime, {name: 'setTotalTime'}),
        graphql(setCorrectCount, {name: 'setCorrectCount'}),
        graphql(currentTotalTime, {name: 'currentTotalTime'})
    )(withStyles(styles)(Learning)))