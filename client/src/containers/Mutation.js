import gql from 'graphql-tag';

const ADD_KANJI = gql`
mutation ADD_KANJI($character: String!, $hiragana: String!) {
    addkanji(character: $character, hiragana: $hiragana) {
        character
        hiragana
  }
}
`
const UPDATE_KANJI = gql`
mutation UPDATE_KANJI($id: ID!, $character: String!, $hiragana: String!) {
    updatekanji(id: $id, character: $character, hiragana: $hiragana ) {
        id
        character
        hiragana
  }
}
`
const DELETE_KANJI = gql`
mutation DELETE_KANJI($id: ID) {
    deletekanji(id: $id) {
        id
  }
}
`
const LOGIN = gql`
mutation LOGIN($email: String!, $password: String!) {
    login(email: $email, password: $password) {
        token
        user {
            email
        }
  }
}
`

const SIGNUP = gql`
mutation SIGNUP($email: String!, $password: String!, $name: String!) {
    signup(email: $email, password: $password, name: $name) {
        token
        user {
            email
        }
  }
}
`
const storeAuth = gql`
mutation storeAuth($email: String!, $token: String!) {
    storeAuth(email: $email, token: $token) @client{
        token
  }
}
`

const updateKanjis = gql`
mutation updateKanjis($kanjis: [Kanji!]!) {
    updateKanjis(kanjis: $kanjis) @client {
       kanjis
  }
}
`
const increaseCount = gql`
mutation increaseCount($count: Int!) {
  increaseCount(count: $count) @client {
      count
  }
}
`
const setTotalTime = gql`
mutation setTotalTime($totalTime: Int!) {
  setTotalTime(totalTime: $totalTime) @client {
      totalTime
 }
}
`
const setCorrectCount = gql`
mutation setCorrectCount($correctCount: Int!) {
    setCorrectCount(correctCount: $correctCount) @client {
        correctCount
 }
}
`
const resetToDefault = gql`
mutation resetToDefault($correctCount: Int!,$totalTime: Int!, $count:Int!) {
    resetToDefault(correctCount: $correctCount, totalTime: $totalTime, count: $count) @client {
        correctCount,
        totalTime,
        count
 }
}
`

export {
    increaseCount,
    setTotalTime,
    setCorrectCount,
    resetToDefault,
    updateKanjis,
    ADD_KANJI,
    DELETE_KANJI,
    UPDATE_KANJI,
    LOGIN,
    SIGNUP,
    storeAuth
}