// import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
// import logger from 'redux-logger';
// import welcomeReducer from './reducers/welcomeReducer';
// import learningReducer from './reducers/learningReducer';
// import scoreReducer from './reducers/scoreReducer';

// export default function configStore(initialState = {}) {
//     const rootReducer = combineReducers({
//         welcomeReducer,
//         learningReducer,
//         scoreReducer
//     })
//     const store = createStore(
//         rootReducer,
//         initialState,
//         compose(
//             applyMiddleware(
//                 logger,
//             )
//         )
//     );

//     if (module.hot) {
//         module.hot.accept(() => {
//             const nextRootReducer = rootReducer;
//             store.replaceReducer(nextRootReducer);
//         });
//     }

//     return store;
// }