export default {
    Mutation: {
      increaseCount: (_, { count }, { cache }) => {
        cache.writeData({ data: { count } });
        return { count, __typename: 'nextKanji' };
      },
      setTotalTime: (_, { totalTime }, { cache }) => {
        cache.writeData({ data: { totalTime } });
        return { totalTime, __typename: 'totalTime' };
      },
      setCorrectCount: (_, { correctCount }, { cache }) => {
        cache.writeData({ data: { correctCount } });
        return { correctCount, __typename: 'correctCount' };
      },
      resetToDefault: (_, { correctCount, totalTime, count }, { cache }) => {
        cache.writeData({ data: { correctCount, totalTime, count } });
        return null;
      },
      storeAuth: (_, { email, token }, { cache }) => {
        cache.writeData({ data: { email, token } });
        return null;
      },
    }
  };