import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from "react-router-dom";
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
// 1
import { ApolloProvider } from 'react-apollo'
import { ApolloClient } from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

import { split, ApolloLink } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'

import { withClientState } from 'apollo-link-state';
import { setContext } from 'apollo-link-context'
import resolvers from './resolvers';
import { AUTH_TOKEN, LOGIN_ERROR, LOGIN_PASS_ERROR, ADD_KANJI_ERROR} from './constants';
import { onError } from "apollo-link-error";

import { SnackbarProvider } from 'notistack';

const wsLink = new WebSocketLink({
    uri: `ws://localhost:4000`,
    options: {
      reconnect: true
    }
})

const authLink = setContext((_, { headers }) => {
    const token = localStorage.getItem(AUTH_TOKEN)
    return {
      headers: {
        ...headers,
        authorization: token ? `Bearer ${token}` : ''
      }
    }
})

const initialState = {
    count: 0,
    totalTime: 0,
    correctCount: 0
};

const stateLink = withClientState({
    cache: new InMemoryCache(),
    defaults: initialState,
    resolvers: resolvers,
});

const httpLink = createHttpLink({
    uri: 'http://localhost:4000'
})

const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.map(({ message, locations, path }) =>{
        if (message === 'No such user found!'){
            localStorage.setItem(LOGIN_ERROR, message)
        }
        if (message === 'Invalid password!'){
            localStorage.setItem(LOGIN_PASS_ERROR, message)
        }
        localStorage.setItem(ADD_KANJI_ERROR, message)
        console.log(
            `[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`
        )
      }
      );
    if (networkError) console.log(`[Network error]: ${networkError}`);
});

const ehLink = ApolloLink.from([
    authLink,
    errorLink,
    httpLink,
  ]);

const link = split(
    ({ query }) => {
      const { kind, operation } = getMainDefinition(query)
      return kind === 'OperationDefinition' && operation === 'subscription'
    },
    wsLink,
    stateLink.concat(ehLink)
)

const client = new ApolloClient({
    link,
    // link: ApolloLink.from([errorLink, link, authLink]),
    cache: new InMemoryCache()
})

ReactDOM.render(
    <Router>
      <ApolloProvider client={client}>
        <SnackbarProvider maxSnack={3}>
            <App />
        </SnackbarProvider>
      </ApolloProvider>
    </Router>,
    document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
