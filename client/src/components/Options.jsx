import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    fontSize: 30,
    width:200
  },
  buttonHighlighted: {
    margin: theme.spacing.unit,
    fontSize: 30,
    width:200,
    backgroundColor:'#abcb00',
    color: 'white'
  },
  input: {
    display: 'none',
  },
});

const Options = (props) => {
  const { classes, text, action, answer, correctAnswer, shouldHighlight, count } = props;
  return (
    <div>
      <Button disabled={shouldHighlight} variant="outlined" className={answer === correctAnswer && shouldHighlight ? classes.buttonHighlighted : classes.button} onClick={() => action(answer, correctAnswer, count)}>
        {text}
      </Button>
    </div>
  );
}

Options.propTypes = {
  classes: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  action: PropTypes.func.isRequired,
  correctAnswer: PropTypes.number.isRequired,
  shouldHighlight: PropTypes.bool.isRequired,
  count: PropTypes.number.isRequired
};

export default withStyles(styles)(Options);