import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const TimeCounter = props => {
    const { sec } = props;
    let displaySec = sec;
    if (displaySec < 10) {
        displaySec = '0' + displaySec;
    }
    return (
        <div>
            <Grid container>
                <Grid item>
                    <Typography gutterBottom variant="h4">
                        0
                        </Typography>
                </Grid>
                <Grid item>
                    <Typography gutterBottom variant="h4">
                        0
                        </Typography>
                </Grid>
                <Grid item>
                    <Typography gutterBottom variant="h4">
                        :
                        </Typography>
                </Grid>
                <Grid item>
                    <Typography gutterBottom variant="h4">
                        {displaySec}
                    </Typography>
                </Grid>
            </Grid>

        </div>
    );
}

TimeCounter.propTypes = {
    sec: PropTypes.number.isRequired
};

export default TimeCounter;