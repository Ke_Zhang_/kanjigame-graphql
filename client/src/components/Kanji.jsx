import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

const styles = {
    card: {
        minWidth: 200,
        minHeight:200,
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
    },
    cardHighlighted: {
        minWidth: 200,
        minHeight:200,
        display:'flex',
        justifyContent:'center',
        alignItems:'center',
        backgroundColor:'#f2105a',
        color: 'white'
    }
};

class Kanji extends Component {
    render() {
        const { classes, text, shouldHighlight } = this.props;
        return (
            <Card className={shouldHighlight ? classes.cardHighlighted :classes.card }>
                <CardContent>
                    <Typography variant="h2" component="h2" color='inherit'>
                        {text}
                    </Typography>
                </CardContent>
            </Card>
        );
    }
}

Kanji.propTypes = {
    classes: PropTypes.object.isRequired,
    text: PropTypes.string.isRequired,
    shouldHighlight: PropTypes.bool.isRequired
};

export default withStyles(styles)(Kanji);