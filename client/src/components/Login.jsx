import React from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import Typography from '@material-ui/core/Typography';

const Login = (props) => {
  const { classes, change, signIn, email, password, onChange, handleSubmit, emailError, passwordError } = props;
  return (
    <div>
        <form className={classes.form} noValidate onSubmit={(e) => handleSubmit(e)}>
            <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            value={email}
            onChange={(e) => onChange('email', e)}
            error={emailError.length > 0}
            helperText={emailError === '' ? '' : emailError}
            />
            <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            value={password}
            onChange={(e) => onChange('password', e)}
            error={passwordError.length > 0}
            helperText={passwordError === '' ? '' : passwordError}
            />
            <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
            />
            <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className={classes.submit}
            >
            Sign In
            </Button>
            <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            onClick={() => change(!signIn)}
          >
            Don't have an account? Sign Up
          </Button>
        </form>
    </div>
  );
}

export default Login;