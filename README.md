# Language Learning Game 

The app contains a React project as client, Express project as server, and using docker to simplify the building process in different local environments. Mainly technologies include React.js, Apollo-Client, Apollo-state-link, GraphQLServer based on Apollo-server, Graphql.

The project is for demo purpose and may change its tech stacks at any time.

![alt text](https://bitbucket.org/Ke_Zhang_/kanjigame-graphql/raw/1ea8ea76bec0a23f454f338757497d10b23297b8/images/dic.png)

![alt text](https://bitbucket.org/Ke_Zhang_/kanjigame-graphql/raw/ff182dbe48a674b504e9e23b14908926682fe090/images/home.png)

![alt text](https://bitbucket.org/Ke_Zhang_/kanjigame-graphql/raw/ff182dbe48a674b504e9e23b14908926682fe090/images/learn.png)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

What things you need to install the software and how to install them

```
docker
node

```

### Installing

A step by step series of examples that tell you how to get a development env running

Get into project root

```
docker-compose up --build
```
Client: http://localhost:3000
Server: http://localhost:4000

In the case your local env has no docker available

1. Running the server, getting into /server

```
npm install
```
```
npm run watch:server
```
2. Running the client, getting into /client, please use yarn for client, as currently some issues are occurred with NPM

```
yarn install
```
```
yarn start
```
Client: http://localhost:3000
Server: http://localhost:4000

It is always recommended to run within docker container to make sure all dependencies are correct

## Running the tests

No unit tests are written at this stage

### Break down into end to end tests

No E2E tests are written at this stage

## Deployment

I'm going to deploy this web app to AWS Elastic Beanstalk's multi-containers platform.

## TO-DOs

Will deploy multi-containers to AWS instance

## Built With

* [Apollo](https://www.apollographql.com/) - THE DATA GRAPH PLATFORM
* [GraphQL](https://graphql.org/learn/) - A query language for your API
* [React](https://reactjs.org/) - A JavaScript library for building user interfaces
* [Express](https://expressjs.com) - Fast, unopinionated, minimalist web framework for Node.js
* [Prisma](https://www.prisma.io/) - Database tools for modern application development
* [Apollo Client Developer Tools](https://chrome.google.com/webstore/detail/apollo-client-developer-t/jdkknkkbebbapilgoeccciglkfbmbnfm) - GraphQL debugging tools for Apollo Client in the Chrome developer console.

## Contributing

PR welcome!!

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Ke Zhang** - *Front End developer at Nine* - [Nine Entertainment Co](https://www.nineentertainmentco.com.au/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details


