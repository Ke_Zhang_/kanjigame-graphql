function postedBy(parent, args, context) {
    return context.prisma.kanji({ id: parent.id }).postedBy()
}

module.exports = {
    postedBy
}