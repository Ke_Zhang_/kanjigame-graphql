function newKanjiSubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.kanji({ mutation_in: ['CREATED'] }).node()
}

function deleteKanjiSubscribe(parent, args, context, info) {
  return context.prisma.$subscribe.kanji({ mutation_in: ['DELETED'] }).previousValues()
}
  
const newKanji = {
    subscribe: newKanjiSubscribe,
    resolve: payload => {
      return payload
  },
}

const deletedKanji = {
  subscribe: deleteKanjiSubscribe,
  resolve: payload => {
    console.log(payload)
    return payload
},
}
  
  module.exports = {
    newKanji,
    deletedKanji
  }