const { selectQuestions } = require('../utils')

function kanjis(parent, args, context, info) {
    return context.prisma.kanjis()
}

async function pickKanjis(parent, args, context, info) {
    const knajis = await context.prisma.kanjis()
    return selectQuestions(knajis)
}
  
module.exports = {
    kanjis,
    pickKanjis
}