function myKanjis(parent, args, context) {
    return context.prisma.user({ id: parent.id }).myKanjis()
}

module.exports = {
    myKanjis
}