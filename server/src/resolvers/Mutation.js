const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils')
// import { AuthenticationError } from 'apollo-server';
const { AuthenticationError, UserInputError } = require('apollo-server');


async function addkanji(parent, args, context, info) {
    const userId = getUserId(context)
    if (!userId) {
        throw new AuthenticationError('user not authenticated to do the action!')
    }
    const kanjiExists = await context.prisma.$exists.kanji({
        character: args.character,
    })
    if (kanjiExists) {
        throw new UserInputError(`character already in database: ${args.character}`)
    }
    return context.prisma.createKanji({
        character: args.character,
        hiragana: args.hiragana,
        postedBy: { connect: { id: userId } }
    })
}

function deletekanji(parent, args, context, info) {
    return context.prisma.deleteKanji({
        id: args.id
    })
}

function updatekanji(parent, args, context, info) {
    return context.prisma.updateKanji({
        data: {
            character: args.character,
            hiragana: args.hiragana,
        },
        where: {
            id: args.id,
        },
    })
}

async function signup(parent, args, context, info) {
    // 1
    const password = await bcrypt.hash(args.password, 10)
    // 2
    const user = await context.prisma.createUser({ ...args, password })
  
    // 3
    const token = jwt.sign({ userId: user.id }, APP_SECRET)
  
    // 4
    return {
      token,
      user,
    }
  }
  
  async function login(parent, args, context, info) {
    // 1
    const user = await context.prisma.user({ email: args.email })
    if (!user) {
        throw new AuthenticationError('No such user found!')
    }
  
    // 2
    const valid = await bcrypt.compare(args.password, user.password)
    if (!valid) {
      throw new AuthenticationError('Invalid password!')
    }
  
    const token = jwt.sign({ userId: user.id }, APP_SECRET)
  
    // 3
    return {
      token,
      user,
    }
  }

module.exports = {
    addkanji,
    deletekanji,
    updatekanji,
    signup,
    login
}