const jwt = require('jsonwebtoken')
const APP_SECRET = 'Hello Ke'

function getUserId(context) {
    const Authorization = context.request.get('Authorization')
    if (Authorization) {
      const token = Authorization.replace('Bearer ', '')
      const { userId } = jwt.verify(token, APP_SECRET)
      return userId
    }
  
    throw new Error('Not authenticated')
}

const selectQuestions = (data) => {
    let max = data.length;
    let randomIndex = [];
    let selectedQuestions = [];
    while(randomIndex.length < 10){
        let num = Math.floor(Math.random() * Math.floor(max));
        if(randomIndex.indexOf(num) === -1) randomIndex.push(num);
    }
    for(let i = 0; i < randomIndex.length; i++){
        // remove current characters from all data
        let cloneHiragana = [];
        let optionIndex = [0,1,2];
        let otherOption = [];
        let allOption = [];
        let cloneData = [...data];
        cloneData.splice(randomIndex[i], 1);
        // pick two random options from the remaining data
        let maxClone = cloneData.length;
        for (let a = 0; a < cloneData.length; a++){
            cloneHiragana.push(cloneData[a].hiragana);
        }
        while(otherOption.length < 2){
            let numClone = Math.floor(Math.random() * Math.floor(maxClone));
            if(otherOption.indexOf(cloneHiragana[numClone]) === -1) otherOption.push(cloneData[numClone].hiragana);
        }
        let correctIndex = Math.floor(Math.random() * Math.floor(3));
        allOption[correctIndex] = data[randomIndex[i]].hiragana;
        for(let j = 0; j < optionIndex.length; j++){
            if(optionIndex[j] !== correctIndex && otherOption.length === 2){
                allOption[optionIndex[j]] = otherOption[0];
                otherOption.splice(0,1);
            } else if (optionIndex[j] !== correctIndex && otherOption.length === 1){
                allOption[optionIndex[j]] = otherOption[0];
            }
        }
        selectedQuestions[i] = {
            character:data[randomIndex[i]].character,
            options:allOption,
            correctIndex:correctIndex
        }
    }
    return selectedQuestions
}

module.exports =  {
    selectQuestions,
    APP_SECRET,
    getUserId
}